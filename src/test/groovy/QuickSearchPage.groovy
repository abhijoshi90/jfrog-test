import geb.Page

class QuickSearchPage extends Page {
    static url = "http://localhost:8020/artifactory/webapp/#/search/quick/"

    static at = {
        title.contains("Artifactory")
        waitFor { searchHeader }
        searchHeader.text().contains("Search Artifacts")
    }

    static content = {
        searchHeader { $("div.search-page-wrapper") }
        searchTextbox { $("#criterion-string-0") }
        searchButton { $("div.action-buttons button.btn-primary") }
        warningToast { $("div.toast-top-center div.toast-warning") }
        searchResult { $("div.autotest-quick-artifact") }
    }

    boolean findFile(String targetFileName) {
        for(int i=0; i< searchResult.size(); i++) {
            if(searchResult[i].text().contains(targetFileName))
                return true
        }
        return false
    }
}
