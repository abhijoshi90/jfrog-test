import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Paths

class RestHelper {
    static boolean createFileInLocalRepo(String baseUrl,
                                         String username,
                                         String password,
                                         String repoName,
                                         String sourceFileName,
                                         String targetFileName) {
        try {
            URL url = new URL("http://localhost:8020/artifactory/" + repoName + "/" + targetFileName);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            String auth = username + ":" + password;
            conn.setRequestProperty("Authorization", "Basic " + Base64.getEncoder().encodeToString(
                    auth.getBytes(StandardCharsets.UTF_8)));

            conn.setRequestMethod("PUT");
            conn.setDoOutput(true);
            OutputStream out = conn.getOutputStream()
            Files.copy(Paths.get(sourceFileName), out)
            if (!(conn.getResponseCode() == HttpURLConnection.HTTP_CREATED)) {
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
