package com.jfrog.test

import geb.spock.GebSpec
import spock.lang.Shared

class JfrogTest extends GebSpec {
    String username = "admin"
    String password = "jfrog@123"
    @Shared
    String targetFileName = "sample-" + System.currentTimeMillis() + ".txt"
    @Shared
    String sourceFileName = "src/test/resources/sample.txt"

    def setupSpec() {

        RestHelper.createFileInLocalRepo(baseUrl, "admin", "jfrog@123",
                "example-repo-local", sourceFileName, targetFileName)
    }

    def "incorrect password should NOT login successfully"() {
        when:
        to LoginPage

        and:
        login(username, password + "4")

        then:
        at LoginPage
        waitFor { loginError.displayed }
        loginError.text().contains("Username or password is incorrect") == true
        loginButton.present == true
    }

    def "correct username and password should login successfully"() {
        when:
        to LoginPage

        and:
        login(username, password)

        then:
        at HomePage
        waitFor { welcomeDropdown.displayed }
        welcomeDropdown.text().contains(username) == true
    }

    def "navigate to quick search with bad search"() {
        when:
        to LoginPage

        and:
        login(username, password)

        then:
        at HomePage

        when:
        searchArtifacts("ample")

        then:
        at QuickSearchPage
        waitFor { searchHeader.displayed }
        searchHeader.text().contains("Search Artifacts") == true
        warningToast.displayed == true
    }

    def "navigate to quick search with right search"() {
        when:
        to LoginPage

        and:
        login(username, password)

        then:
        at HomePage

        when:
        searchArtifacts("sample")

        then:
        at QuickSearchPage
        waitFor { searchHeader.displayed }
        waitFor { searchResult }
        findFile(targetFileName)

    }
}