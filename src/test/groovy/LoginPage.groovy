import geb.Page

class LoginPage extends Page {

    static at = { title.contains("Artifactory") }

    static content = {
        username { $("#user") }
        password { $("#password") }
        loginButton { $("#login") }
        loginError { $("div.jf-form-errors") }
    }

    void login(String username, String password) {
        this.username = username
        this.password = password
        loginButton.click()
    }
}
