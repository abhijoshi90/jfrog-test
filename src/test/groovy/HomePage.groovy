import geb.Page

class HomePage extends Page {

    static at = {
        title.contains("Artifactory")
        waitFor { welcomeDropdown }
    }

    static content = {
        welcomeDropdown { $("div.user-header-section a.username-header") }
        logoutButton { $("div.quick-actions-wrapper li.menu-item-icon-logout") }
        quickSearchInput { $("#quick") }
    }

    void searchArtifacts(String search) {
        waitFor { quickSearchInput.displayed }
        quickSearchInput = search + "\n"
    }
}
