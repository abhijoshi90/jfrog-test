import org.openqa.selenium.chrome.ChromeDriver

baseUrl = "http://localhost:8020/artifactory/webapp/"

environments {
    chrome {
        driver = { new ChromeDriver() }
    }
}

waiting {
    timeout = 10
}